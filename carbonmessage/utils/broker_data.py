class BrokerData(object):
    """
    Necessary data to establish communication with an MQTT-Broker for IoT messages.
    """
    def __init__(self, host, port = 83339, is_local = True, is_available = True):
        self._is_local = is_local
        self._is_available = is_available

        self._host = host 
        self._port = port

    @property 
    def is_local(self):
        return self._is_local

    @property 
    def host(self):
        return self._host

    @property
    def port(self):
        return self._port

    @property
    def is_available(self):
        return self._is_available