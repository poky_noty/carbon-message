from configparser import ConfigParser, ExtendedInterpolation

from .default_section_data import DefaultSectionData 
from .broker_data import BrokerData

default_section = None
local_broker_data = None
central_broker_data = None 

def read_config( file_name ):
    """
    Read external configuration file and assign data to local variables.
    """
    config = ConfigParser(  interpolation = ExtendedInterpolation() ) 

    response = config.read( file_name )
    if len(response) == 0:
        # Nothing read .. aborting
        raise Exception( 'Wrong Filename ==> Nothing Read & Aborting !' )

    # Is-Daemon ?
    is_daemon = config.getboolean( "default", "is-daemon" )
    pid = config.get( "default", "pid" )
    
    # Logging
    log_filename = config.get( "default", "log-filename" )
    log_level = config.get( "default", "log-level" )
    msg_format = config.get( "default", "msg-format" )
    max_bytes = config.getint( "default", "max-bytes" )
    backup_count = config.getint( "default", "backup-count" )

    global default_section
    default_section = DefaultSectionData(
        is_daemon = is_daemon,
        pid = pid,
        log_filename = log_filename,
        log_level = log_level,
        msg_format = msg_format,
        max_bytes = max_bytes,
        backup_count = backup_count
    )

    # Broker - Local
    is_available = config.getboolean( "local-broker", "is-available" )
     
    host = config.get( "local-broker", "host" )
    port = config.getint( "local-broker", "port" )

    global local_broker_data
    local_broker_data = BrokerData(is_local = True, is_available = is_available, port = port, host = host)
    
    # Broker - Central 
    _is_available = config.getboolean( "central-broker", "is-available" )
     
    _host = config.get( "central-broker", "host" )
    _port = config.getint( "central-broker", "port" )

    global central_broker_data
    central_broker_data = BrokerData(is_local = False, is_available = _is_available, port = _port, host = _host)
    
def get_section( name ):
    if name.lower() == "default":
        # [default]
        return default_section
    elif name.lower() == "local-broker":
        return local_broker_data
    elif name.lower() == "central-broker":
        return central_broker_data
    else:
        # Unknown section 
        raise Exception ( "Section Unknown" ) 
