import logging
from logging.handlers import RotatingFileHandler 

LEVELS = {
    "CRITICAL": logging.CRITICAL,
    "ERROR": logging.ERROR,
    "WARNING": logging.WARNING,
    "INFO": logging.INFO,
    "DEBUG": logging.DEBUG,
}

# Global object
logger = None

def log_setup( filename, level, maxBytes, backupCount, msg_format, name = "steel-rain" ):
    global logger 
        
    logger = logging.getLogger( name )
    logger.setLevel( LEVELS[ level ] )
    
    # sudo chmod 777 /var/log/nmapwrapper/nmapwrapper.log 
    fh = RotatingFileHandler( filename = filename, maxBytes = maxBytes * 1024 * 1024, backupCount = backupCount )
    
    formatter = logging.Formatter( msg_format )
    fh.setFormatter( formatter )

    logger.addHandler( fh )

def get_logger():
    global logger
    return logger
