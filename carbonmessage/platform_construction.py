from .utils.external_configuration import read_config, get_section
from .utils.local_logger import log_setup, get_logger

class PlatformConstruction(object):
    def __init__(self, conf_filename = "/etc/carbonmessage/carbonmessage.conf"):
        self.conf_filename = conf_filename

    def apply_external_configuration(self):
        #print( "Ready to Read External Configuration ..." )

        read_config( self.conf_filename )

        self._default_section = get_section( "default" )
        
        self._local_broker = get_section( "local-broker" )
        self._central_broker = get_section( "central-broker" )
        
        #print( "Default Section Data is ...{}".format( self.default_section.pid ) )

    def prepare_logger(self):
        log_setup( 
            filename = self.default_section.log_filename, 
            level = self.default_section.log_level,  
            msg_format = self.default_section.msg_format,
            maxBytes = self.default_section.max_bytes,
            backupCount = self.default_section.backup_count
        )

        self.logger = get_logger() 

    def turn_key_solution(self):
        #print( "Ready to Start MQTT Platform Construction !" )
        self.apply_external_configuration()
        self.prepare_logger()

    def debug(self, message):
        self.logger.debug( message )

    def error(self, message): 
        self.logger.error( message )

    def info(self, message):
        self.logger.info( message )

    @property 
    def local_broker(self):
        return self._local_broker

    @property
    def central_broker(self):
        return self._central_broker

    @property
    def default_section(self):
        return self._default_section
    