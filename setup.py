from setuptools import setup

#
###
setup(
  name="carbon-message",
  version="1.0.0",
  description="Python3 module scanning new messages in mqtt-brokers and sending emails in case of important events",
  keywords="mqtt-broker iot messages abnomaly-detection notifications",
  # Project's main page 
  url="https://gitlab.com/poky_noty/carbon-message",
  # Author details
  author="Miltos K. Vimplis", 
  author_email="mvimblis@gmail.com",
  # Choose your license
  license="MIT License",
  classifiers=[
    "Programing Language :: Python :: 3.7",
    "Operating System :: Linux", 
    "Environment :: Console"
  ],
  packages=["carbonmessage", "carbonmessage.utils", "carbonmessage.telegraph"],
  #install_requires=[""], 
  entry_points={
    "console_scripts": [
      "steel-rain=carbonmessage.server:main_schedule",
     ],
  },
  zip_safe=True
)
